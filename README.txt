Almost Administer Content
=========================

Description
-----------

Lightweight module allowing specific roles to edit a post's publishing options and authoring information etc. without needing the "Administer content" permission.

It is very similar to the Override Node Options module but only adds one new option to the permissions table for each content type.


Installation
------------

* Install as usual, see https://www.drupal.org/documentation/install/modules-themes/modules-7 for further information.


Configuration
-------------

* Go to admin/people/permissions and grant permission to any roles that you want to be able to change the publishing, authoring information, revisions and comment settings options on node add/edit forms